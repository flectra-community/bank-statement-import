# Flectra Community / bank-statement-import

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[account_statement_import_file_reconciliation_widget](account_statement_import_file_reconciliation_widget/) | 2.0.1.0.0| Import Statement Files and Go Direct to Reconciliation
[account_statement_import_camt](account_statement_import_camt/) | 2.0.1.2.1| CAMT Format Bank Statements Import
[account_statement_import_ofx_by_acctid](account_statement_import_ofx_by_acctid/) | 2.0.1.0.0|         Import OFX Bank Statement by ACCTID
[account_statement_import_move_line](account_statement_import_move_line/) | 2.0.1.0.0| Import journal items into bank statement
[account_statement_import_base](account_statement_import_base/) | 2.0.1.0.0| Base module for Bank Statement Import
[account_statement_import_online](account_statement_import_online/) | 2.0.4.0.0| Online bank statements update
[account_statement_import](account_statement_import/) | 2.0.3.3.2| Import Statement Files
[account_statement_import_online_ponto_ing](account_statement_import_online_ponto_ing/) | 2.0.1.0.0| Online Bank Statements: MyPonto.com ING customization
[account_statement_import_online_paypal](account_statement_import_online_paypal/) | 2.0.1.0.0| Online bank statements for PayPal.com
[account_statement_import_camt54](account_statement_import_camt54/) | 2.0.1.1.0| Bank Account Camt54 Import
[account_statement_import_online_wise](account_statement_import_online_wise/) | 2.0.1.0.0| Online bank statements for Wise.com (TransferWise.com)
[account_statement_import_paypal](account_statement_import_paypal/) | 2.0.1.0.1| Import PayPal CSV files as Bank Statements in Odoo
[account_statement_import_ofx](account_statement_import_ofx/) | 2.0.1.0.0| Import OFX Bank Statement
[account_statement_import_txt_xlsx](account_statement_import_txt_xlsx/) | 2.0.3.1.0| Import TXT/CSV or XLSX files as Bank Statements in Odoo
[account_statement_import_online_ponto](account_statement_import_online_ponto/) | 2.0.2.0.0| Online Bank Statements: MyPonto.com


