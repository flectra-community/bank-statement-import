# Copyright 2021 Therp BV <https://therp.nl>.
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Online Bank Statements: MyPonto.com ING customization",
    "version": "2.0.1.0.0",
    "category": "Account",
    "website": "https://gitlab.com/flectra-community/bank-statement-import",
    "author": "Therp BV, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "installable": True,
    "depends": ["account_statement_import_online_ponto"],
}
