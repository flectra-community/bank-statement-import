# Copyright 2013-2016 Therp BV <https://therp.nl>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
import logging
import zipfile
from collections import defaultdict
from io import BytesIO

from flectra import models
from flectra.tools import float_compare

_logger = logging.getLogger(__name__)


class AccountBankStatementImport(models.TransientModel):
    _inherit = "account.statement.import"

    def import_file_button(self):
        action = super(AccountBankStatementImport, self).import_file_button()
        ctx = action.get('context')
        if self.env.context.get("return_regular_interface_action") or not ctx:
            return action

        line_ids = ctx.get('statement_line_ids')
        statements = self.env['account.bank.statement.line'].browse(line_ids).mapped('statement_id')
        if statements.filtered(lambda f: f.state == 'open'):
            action = (
                self.env.ref("account.action_bank_statement_tree").sudo().read([])[0]
            )
            if len(statements) == 1:
                action.update(
                        {
                            "view_mode": "form,tree",
                            "views": False,
                            "res_id": statements.id,
                        }
                )
            else:
                action["domain"] = [("id", "in", statements.ids)]

        return action

    def import_single_file(self, file_data, result):
        super(AccountBankStatementImport, self).import_single_file(file_data, result)
        self._merge_camt054_with_camt053(result)

    def _merge_camt054_with_camt053(self, result):

        camt054_statements = self.env['account.bank.statement']
        selected_statements = self.env['account.bank.statement']
        incomplete_statements = self.env['account.bank.statement']

        lines = self.env['account.bank.statement.line'].search([
            ('state', '=', 'open'),
            ('btch_msgid', '!=', False),
            ('is_reconciled', '=', False),
            ('statement_id.journal_id.use_camt054', '=', True)
        ])

        if not lines:
            selected_statements = selected_statements.browse(result['statement_ids'])

        camt053_lines = {}
        camt054_lines = defaultdict(list)
        for line in lines:
            if line.is_camt054:
                camt054_lines[line.btch_msgid].append(line)
            else:
                camt053_lines[line.btch_msgid] = line

        for key, camt053 in camt053_lines.items():
            selected_statements |= camt053.statement_id
            related_entries = camt054_lines.get(key)
            if not related_entries:
                incomplete_statements |= camt053.statement_id
                continue
            entries = self.env['account.bank.statement.line']
            for rel in related_entries:
                entries |= rel
            entry_total = sum(entries.mapped('amount'))
            if float_compare(entry_total, camt053.amount, precision_digits=5) != 0:
                continue
            camt054_statements |= entries.mapped('statement_id')
            self.env['ir.attachment'].search([
                ('res_model', '=', 'account.bank.statement'),
                ('res_id', 'in', camt054_statements.ids),
            ]).write({'res_id': camt053.statement_id.id})
            entries.write({
                'statement_id': camt053.statement_id.id,
                'sequence': camt053.sequence,
            })
            if camt053.statement_id.id not in result['statement_ids']:
                result['statement_ids'].append(camt053.statement_id.id)
            camt053.unlink()

        empty_camt054_statements = camt054_statements.filtered(lambda f: not f.line_ids)
        used_camt054_statements = camt054_statements.filtered(lambda f: f.line_ids)
        for empty_statement in empty_camt054_statements:
            if empty_statement.id in result['statement_ids']:
                result['statement_ids'].remove(empty_statement.id)
        for used_statement in used_camt054_statements:
            if used_statement.id not in result['statement_ids']:
                result['statement_ids'].append(used_statement.id)
        empty_camt054_statements.unlink()

        completed_statements = selected_statements - incomplete_statements
        try:
            completed_statements.button_post()
        except:
            pass
