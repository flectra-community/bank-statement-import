import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    btch_msgid = fields.Char(
            string='Batch Message ID',
            translate=False,
            readonly=True,
    )

    is_camt054 = fields.Boolean(
            string='Is CAMT.054',
    )
