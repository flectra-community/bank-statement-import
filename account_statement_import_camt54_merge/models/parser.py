"""Class to parse camt files."""
# Copyright 2013-2016 Therp BV <https://therp.nl>
# Copyright 2017 Open Net Sàrl
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
import re

from lxml import etree

from flectra import models


class CamtParser(models.AbstractModel):
    _inherit = "account.statement.import.camt.parser"

    def parse_entry(self, ns, node, currency):
        transactions = list(super(CamtParser, self).parse_entry(ns, node, currency))
        for transaction in transactions:
            self.add_value_from_node(
                    ns,
                    node,
                    "./ns:NtryDtls/ns:Btch/ns:MsgId",
                    transaction,
                    "btch_msgid",
            )
            if not transaction.get('btch_msgid') and transaction.get('is_camt054'):
                transaction['is_camt054'] = False
        return transactions

    def parse_transaction_details(self, ns, node, transaction, currency):
        super(CamtParser, self).parse_transaction_details(ns, node, transaction, currency)

        transaction['is_camt054'] = True

        # name
        self.add_value_from_node(
                ns, node, [
                    "./ns:RmtInf/ns:Strd/ns:CdtrRefInf/ns:Ref",
                    "./ns:AddtlTxInf"
                ], transaction, "payment_ref", join_str="\n"
        )
        # eref
        self.add_value_from_node(
                ns,
                node,
                [
                    "./ns:Refs/ns:EndToEndId",
                    "./ns:Ntry/ns:AcctSvcrRef",
                ],
                transaction,
                "ref",
        )
