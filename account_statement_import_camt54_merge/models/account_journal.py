import logging
from flectra import models, fields, api, _

_logger = logging.getLogger(__name__)


class AccountJournal(models.Model):
    _inherit = 'account.journal'

    use_camt054 = fields.Boolean(
            string="Use Camt 054",
            default=True,
    )
