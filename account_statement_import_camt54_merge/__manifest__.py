# Copyright 2013-2017 Therp BV <https://therp.nl>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "CAMT.054 Merge with CAMT.053 Batch Lines",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Jamotion GmbH",
    "website": "https://jamotion.ch",
    "category": "Banking addons",
    "depends": ["account_statement_import_camt"],
    "data": [
        "views/account_bank_statement_import.xml",
        "views/account_journal_views.xml",
    ],
}
